﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Chiper
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }
        CHIP_8_Interpreter interpreter;
        Bitmap bmp;
        Stopwatch sw, fpsWatch;
        int fps, cycles;
        bool resizing = false;
        private void Form1_Load(object sender, EventArgs e)
        {
            BringToFront();
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                sw = Stopwatch.StartNew();
                fpsWatch = Stopwatch.StartNew();
                bmp = new Bitmap(64, 32);
                interpreter = new CHIP_8_Interpreter(File.ReadAllBytes(ofd.FileName));
                Thread fpsThread = new Thread(() =>
                {
                    while (true)
                    {
                        if (fpsWatch.Elapsed.TotalMilliseconds >= 1000)
                        {
                            fpsWatch.Restart();
                            Text = fps.ToString() + " FPS " + cycles.ToString() + " Cycles Per Second";
                            fps = cycles = 0;
                        }
                        if (interpreter.drawFlag)
                        {
                            lock (bmp)
                            {
                                BitmapData imageData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
                                IntPtr firstAdress = imageData.Scan0;
                                byte[] oneBitPerPixelArray = new byte[Math.Abs(imageData.Stride) * bmp.Height];
                                Marshal.Copy(firstAdress, oneBitPerPixelArray, 0, oneBitPerPixelArray.Length);
                                for (int i = 0; i < interpreter.gfx.Length; i++)
                                {
                                    oneBitPerPixelArray[i * 3] = (byte)(interpreter.gfx[i] * 0xFF);
                                    oneBitPerPixelArray[i * 3 + 1] = (byte)(interpreter.gfx[i] * 0xFF);
                                    oneBitPerPixelArray[i * 3 + 2] = (byte)(interpreter.gfx[i] * 0xFF);
                                }
                                Marshal.Copy(oneBitPerPixelArray, 0, firstAdress, oneBitPerPixelArray.Length);
                                bmp.UnlockBits(imageData);
                            }
                            pictureBox1.Invalidate();
                            interpreter.drawFlag = false;
                            fps++;
                        }
                    }
                });
                fpsThread.IsBackground = true;
                fpsThread.Start();
                Thread interpreterThread = new Thread(() =>
                {
                    while (true)
                    {
                        if (sw.Elapsed.TotalMilliseconds >= 1)
                        {
                            interpreter.emulateCycle();
                            cycles++;
                            sw.Restart();
                        }
                    }
                });
                interpreterThread.IsBackground = true;
                interpreterThread.Start();
            }
            else
            {
                Application.Exit();
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Q) interpreter.key[0x1] = 1;
            else if (e.KeyData == Keys.W) interpreter.key[0x2] = 1;
            else if (e.KeyData == Keys.E) interpreter.key[0x3] = 1;
            else if (e.KeyData == Keys.R) interpreter.key[0xC] = 1;

            else if (e.KeyData == Keys.A) interpreter.key[0x4] = 1;
            else if (e.KeyData == Keys.S) interpreter.key[0x5] = 1;
            else if (e.KeyData == Keys.D) interpreter.key[0x6] = 1;
            else if (e.KeyData == Keys.F) interpreter.key[0xD] = 1;

            else if (e.KeyData == Keys.Z) interpreter.key[0x7] = 1;
            else if (e.KeyData == Keys.X) interpreter.key[0x8] = 1;
            else if (e.KeyData == Keys.C) interpreter.key[0x9] = 1;
            else if (e.KeyData == Keys.V) interpreter.key[0xE] = 1;

            else if (e.KeyData == Keys.T) interpreter.key[0xA] = 1;
            else if (e.KeyData == Keys.Y) interpreter.key[0x0] = 1;
            else if (e.KeyData == Keys.U) interpreter.key[0xB] = 1;
            else if (e.KeyData == Keys.I) interpreter.key[0xF] = 1;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Q) interpreter.key[0x1] = 0;
            else if (e.KeyData == Keys.W) interpreter.key[0x2] = 0;
            else if (e.KeyData == Keys.E) interpreter.key[0x3] = 0;
            else if (e.KeyData == Keys.R) interpreter.key[0xC] = 0;

            else if (e.KeyData == Keys.A) interpreter.key[0x4] = 0;
            else if (e.KeyData == Keys.S) interpreter.key[0x5] = 0;
            else if (e.KeyData == Keys.D) interpreter.key[0x6] = 0;
            else if (e.KeyData == Keys.F) interpreter.key[0xD] = 0;

            else if (e.KeyData == Keys.Z) interpreter.key[0x7] = 0;
            else if (e.KeyData == Keys.X) interpreter.key[0x8] = 0;
            else if (e.KeyData == Keys.C) interpreter.key[0x9] = 0;
            else if (e.KeyData == Keys.V) interpreter.key[0xE] = 0;

            else if (e.KeyData == Keys.T) interpreter.key[0xA] = 0;
            else if (e.KeyData == Keys.Y) interpreter.key[0x0] = 0;
            else if (e.KeyData == Keys.U) interpreter.key[0xB] = 0;
            else if (e.KeyData == Keys.I) interpreter.key[0xF] = 0;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_ResizeBegin(object sender, EventArgs e)
        {
            resizing = true;
        }

        private void MainForm_ResizeEnd(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
            resizing = false;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (!resizing)
            {
                lock (bmp)
                {
                    e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                    e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
                    e.Graphics.DrawImage((Image)bmp.Clone(), new Rectangle(0, 0, pictureBox1.Width, pictureBox1.Height), 0, 0, bmp.Width, bmp.Height, GraphicsUnit.Pixel);
                }
            }
        }
    }
}

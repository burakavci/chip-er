﻿using System;
using System.Threading;
using System.Diagnostics;

namespace Chiper
{
    class CHIP_8_Interpreter
    {
        public CHIP_8_Interpreter(byte[] GameData)
        {
            Array.Copy(GameData, 0, memory, 0x200, GameData.Length);
            pc = 0x200;
            for (int i = 0; i < 80; i++)
            {
                memory[i] = chip8_fontset[i];
            }
            Thread t = new Thread(() =>
            {
                while (true)
                {
                    if (sw.ElapsedMilliseconds >= hz60)
                    {
                        if (delay_timer > 0)
                        {
                            delay_timer--;
                        }
                        if (sound_timer > 0)
                        {
                            //Console.Beep(5000, 10);
                            sound_timer--;
                        }
                        //count++;
                        //if (sw1.ElapsedMilliseconds >= 1000)
                        //{
                        //    Debug.WriteLine(count.ToString() + " ----- sw0= " + sw.ElapsedMilliseconds + " ----- sw1= " + sw1.ElapsedMilliseconds);
                        //    count = 0;
                        //    sw1.Reset();
                        //    sw1.Start();
                        //}
                        sw.Reset();
                        sw.Start();
                    }
                }
            });
            t.IsBackground = true;
            t.Start();
            //sw1.Start();
        }

        //int count;
        //Stopwatch sw1 = new Stopwatch();
        public const double hz60 = 1000.0 / 60;
        Stopwatch sw = Stopwatch.StartNew();
        Random rand = new Random();
        ushort opcode, I, pc, sp;
        byte[] memory = new byte[4096];
        byte[] V = new byte[16];
        public byte[] gfx = new byte[64 * 32];
        public byte delay_timer;
        byte sound_timer;
        ushort[] stack = new ushort[16];
        public byte[] key = new byte[16];
        public bool drawFlag = false;
        byte[] chip8_fontset = {
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };

        void executeOpcode()
        {
            switch ((opcode & 0xF000) >> 12)
            {
                case 0x0:
                    switch (opcode & 0x000F)
                    {
                        case 0x0000:
                            gfx = new byte[64 * 32];
                            drawFlag = true;
                            break;

                        case 0x000E:
                            sp--;
                            pc = stack[sp];
                            break;
                    }
                    pc += 2;
                    break;

                case 0x1:
                    pc = (ushort)(opcode & 0x0FFF);
                    break;

                case 0x2:
                    stack[sp] = pc;
                    sp++;
                    pc = (ushort)(opcode & 0x0FFF);
                    break;

                case 0x3:
                    if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
                    {
                        pc += 4;
                        return;
                    }
                    pc += 2;
                    break;

                case 0x4:
                    if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
                    {
                        pc += 4;
                        return;
                    }
                    pc += 2;
                    break;

                case 0x5:
                    if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
                    {
                        pc += 4;
                        return;
                    }
                    pc += 2;
                    break;

                case 0x6:
                    V[(opcode & 0x0F00) >> 8] = (byte)(opcode & 0x00FF);
                    pc += 2;
                    break;

                case 0x7:
                    V[(opcode & 0x0F00) >> 8] += (byte)(opcode & 0x00FF);
                    pc += 2;
                    break;

                case 0x8:
                    switch (opcode & 0x000F)
                    {
                        case 0x0000:
                            V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0001:
                            V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0002:
                            V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0003:
                            V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0004:
                            if (V[(opcode & 0x0F00) >> 8] > (255 - V[(opcode & 0x00F0) >> 4]))
                            {
                                V[0x0F] = 1;
                            }
                            else
                            {
                                V[0x0F] = 0;
                            }
                            V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0005:
                            if (V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
                            {
                                V[0x0F] = 0;
                            }
                            else
                            {
                                V[0x0F] = 1;
                            }
                            V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
                            break;

                        case 0x0006:
                            V[0x0F] = (byte)(V[(opcode & 0x0F00) >> 8] & 0x0001);
                            V[(opcode & 0x0F00) >> 8] >>= 1;
                            break;

                        case 0x0007:
                            if (V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])
                            {
                                V[0x0F] = 0;
                            }
                            else
                            {
                                V[0x0F] = 1;
                            }
                            V[(opcode & 0x0F00) >> 8] = (byte)(V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8]);
                            break;

                        case 0x000E:
                            V[0x0F] = (byte)(V[(opcode & 0x0F00) >> 8] >> 7);
                            V[(opcode & 0x0F00) >> 8] <<= 1;
                            break;
                    }
                    pc += 2;
                    break;

                case 0x9:
                    if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
                    {
                        pc += 4;
                        return;
                    }
                    pc += 2;
                    break;

                case 0xA:
                    I = (ushort)(opcode & 0x0FFF);
                    pc += 2;
                    break;

                case 0xB:
                    pc = (ushort)((opcode & 0x0FFF) + V[0]);
                    break;

                case 0xC:
                    V[(opcode & 0x0F00) >> 8] = (byte)(rand.Next(0, 255) & (opcode & 0x00FF));
                    pc += 2;
                    break;

                case 0xD:
                    ushort x = V[(opcode & 0x0F00) >> 8];
                    ushort y = V[(opcode & 0x00F0) >> 4];
                    ushort height = (ushort)(opcode & 0x000F);
                    ushort pixel;
                    V[0xF] = 0;
                    for (int yline = 0; yline < height; yline++)
                    {
                        pixel = memory[I + yline];
                        for (int xline = 0; xline < 8; xline++)
                        {
                            if ((pixel & (0x80 >> xline)) != 0)
                            {
                                if (gfx[(x + xline + ((y + yline) * 64)) % 2048] == 1)
                                {
                                    V[0xF] = 1;
                                }
                                gfx[(x + xline + ((y + yline) * 64)) % 2048] ^= 1;
                            }
                        }
                    }
                    drawFlag = true;
                    pc += 2;
                    break;

                case 0xE:
                    switch (opcode & 0x00FF)
                    {
                        case 0x009E:
                            if (key[V[(opcode & 0x0F00) >> 8]] != 0)
                            {
                                pc += 2;
                            }
                            break;

                        case 0x00A1:
                            if (key[V[(opcode & 0x0F00) >> 8]] == 0)
                            {
                                pc += 2;
                            }
                            break;
                    }
                    pc += 2;
                    break;

                case 0xF:
                    switch (opcode & 0x00FF)
                    {
                        case 0x0007:
                            V[(opcode & 0x0F00) >> 8] = delay_timer;
                            break;
                        case 0x000A:
                            bool keyPress = false;
                            for (int i = 0; i < 16; i++)
                            {
                                if (key[i] != 0)
                                {
                                    V[(opcode & 0x0F00) >> 8] = (byte)(i);
                                    keyPress = true;
                                }
                            }
                            if (!keyPress)
                            {
                                pc -= 2;
                            }
                            break;
                        case 0x0015:
                            delay_timer = V[(opcode & 0x0F00) >> 8];
                            break;
                        case 0x0018:
                            sound_timer = V[(opcode & 0x0F00) >> 8];
                            break;
                        case 0x001E:
                            if (I + V[(opcode & 0x0F00) >> 8] > 0xFFF)
                            {
                                V[0xF] = 1;
                            }
                            else
                            {
                                V[0xF] = 0;
                            }
                            I += V[(opcode & 0x0F00) >> 8];
                            break;
                        case 0x0029:
                            I = (ushort)(V[(opcode & 0x0F00) >> 8] * 0x5);
                            break;
                        case 0x0033:
                            memory[I] = (byte)(V[(opcode & 0x0F00) >> 8] / 100);
                            memory[I + 1] = (byte)((V[(opcode & 0x0F00) >> 8] / 10) % 10);
                            memory[I + 2] = (byte)((V[(opcode & 0x0F00) >> 8] % 100) % 10);
                            break;
                        case 0x0055:
                            for (int i = 0; i <= ((opcode & 0x0F00) >> 8); i++)
                            {
                                memory[I + i] = V[i];
                            }
                            I += (ushort)(((opcode & 0x0F00) >> 8) + 1);
                            break;
                        case 0x0065:
                            for (int i = 0; i <= ((opcode & 0x0F00) >> 8); i++)
                            {
                                V[i] = memory[I + i];
                            }
                            I += (ushort)(((opcode & 0x0F00) >> 8) + 1);
                            break;
                    }
                    pc += 2;
                    break;
            }
        }

        public void emulateCycle()
        {
            //while(sw.ElapsedMilliseconds <= (totalTime + hz60))
            //{

            //}
            //count++;
            //totalTime += hz60;
            opcode = (ushort)(memory[pc] << 8 | memory[pc + 1]);
            executeOpcode();
            //if(sw.ElapsedMilliseconds >= 1000)
            //{
            //    totalTime = 0;
            //    Debug.WriteLine(sw.ElapsedMilliseconds);
            //    sw = Stopwatch.StartNew();
            //    Debug.WriteLine(count);
            //    count = 0;
            //}
        }
    }
}
